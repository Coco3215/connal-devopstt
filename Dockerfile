FROM bitnami/wordpress:5.3.2-debian-10-r32
# Chose this image as it seemed very common

#Not really a good idea, but works for this demo
USER 0
RUN apt-get update && apt-get install -y --no-install-recommends git
#install git so we can clone the repo at start up and grab the neccesasry config

COPY git-clone.sh /usr/local/bin/
# Add clone script

ENTRYPOINT ["git-clone.sh"]
# Run cloning script at start
