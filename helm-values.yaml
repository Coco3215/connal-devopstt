image:
  registry: registry.gitlab.com
  repository: coco3215/connal-devopstt
  tag: 0.0.6

  ## Specify a imagePullPolicy
  ## Defaults to 'Always' if image tag is 'latest', else set to 'IfNotPresent'
  ## ref: http://kubernetes.io/docs/user-guide/images/#pre-pulling-images
  ##
  pullPolicy: IfNotPresent
  ## Optionally specify an array of imagePullSecrets.
  ## Secrets must be manually created in the namespace.
  ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  ##
  # pullSecrets:
  #   - myRegistryKeySecretName
  ## Set to true if you would like to see extra information on logs
  ##
  debug: false

## String to partially override wordpress.fullname template (will maintain the release name)
##
# nameOverride:

## String to fully override wordpress.fullname template
##
# fullnameOverride:

## User of the application
## ref: https://github.com/bitnami/bitnami-docker-wordpress#environment-variables
##
wordpressUsername: user

## Application password
## Defaults to a random 10-character alphanumeric string if not set
## ref: https://github.com/bitnami/bitnami-docker-wordpress#environment-variables
##
# wordpressPassword:

## Admin email
## ref: https://github.com/bitnami/bitnami-docker-wordpress#environment-variables
##
wordpressEmail: user@example.com

## First name
## ref: https://github.com/bitnami/bitnami-docker-wordpress#environment-variables
##
wordpressFirstName: FirstName

## Last name
## ref: https://github.com/bitnami/bitnami-docker-wordpress#environment-variables
##
wordpressLastName: LastName

## Blog name
## ref: https://github.com/bitnami/bitnami-docker-wordpress#environment-variables
##
wordpressBlogName: User's Blog!

## Table prefix
## ref: https://github.com/bitnami/bitnami-docker-wordpress#environment-variables
##
wordpressTablePrefix: wp_

## Scheme to generate application URLs
## ref: https://github.com/bitnami/bitnami-docker-wordpress#environment-variables
##
wordpressScheme: https

## Skip wizard installation (only if you use an external database that already contains WordPress data)
## ref: https://github.com/bitnami/bitnami-docker-wordpress#connect-wordpress-docker-container-to-an-existing-database
##
wordpressSkipInstall: false

## Set up update strategy for wordpress installation. Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to makesure the pods is destroyed first.
## ref: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy
## Example:
## updateStrategy:
##  type: RollingUpdate
##  rollingUpdate:
##    maxSurge: 25%
##    maxUnavailable: 25%
updateStrategy:
  type: RollingUpdate

## Set to `false` to allow the container to be started with blank passwords
## ref: https://github.com/bitnami/bitnami-docker-wordpress#environment-variables
##
allowEmptyPassword: true

## Set Apache allowOverride to None
## ref: https://github.com/bitnami/bitnami-docker-wordpress#environment-variables
##
allowOverrideNone: false

# ConfigMap with custom wordpress-htaccess.conf file (requires allowOverrideNone to true)
customHTAccessCM:

## Use an alternate scheduler, e.g. "stork".
## ref: https://kubernetes.io/docs/tasks/administer-cluster/configure-multiple-schedulers/
##
# schedulerName:

## SMTP mail delivery configuration
## ref: https://github.com/bitnami/bitnami-docker-wordpress/#smtp-configuration
##
# smtpHost:
# smtpPort:
# smtpUser:
# smtpPassword:
# smtpUsername:
# smtpProtocol:

replicaCount: 3

## Additional container environment variables
## Example: Configure SSL for database
## extraEnv:
##   - name: WORDPRESS_DATABASE_SSL_CA_FILE
##     value: /path/to/ca_cert
##
extraEnv: []

## Additional volume mounts
## Example: Mount CA file
## extraVolumeMounts
##   - name: ca-cert
##     subPath: ca_cert
##     mountPath: /path/to/ca_cert
extraVolumeMounts: []

## Additional volumes
## Example: Add secret volume
## extraVolumes:
##  - name: ca-cert
##    secret:
##      secretName: ca-cert
##      items:
##        - key: ca-cert
##          path: ca_cert
extraVolumes: []

## WordPress containers' resource requests and limits
## ref: http://kubernetes.io/docs/user-guide/compute-resources/
##
resources:
  limits: {}
  requests:
    memory: 512Mi
    cpu: 300m

## Affinity for pod assignment
## Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
##
affinity: {}

## Node labels for pod assignment. Evaluated as a template.
## ref: https://kubernetes.io/docs/user-guide/node-selection/
##
nodeSelector: {}

## Tolerations for pod assignment. Evaluated as a template.
## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
##
tolerations: {}

## Pod annotations
## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
##
podAnnotations: {}

## K8s Security Context for WordPress pods
## https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
##
securityContext:
  enabled: true
  fsGroup: 1001
  runAsUser: 1001

## Allow health checks to be pointed at the https port
healthcheckHttps: true

## WordPress pod extra options for liveness and readiness probes
## ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes
livenessProbe:
  enabled: true
  initialDelaySeconds: 120
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 6
  successThreshold: 1
readinessProbe:
  enabled: true
  initialDelaySeconds: 30
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 6
  successThreshold: 1

## If using an HTTPS-terminating load-balancer, the probes may need to behave
## like the balancer to prevent HTTP 302 responses. According to the Kubernetes
## docs, 302 should be considered "successful", but this issue on GitHub
## (https://github.com/kubernetes/kubernetes/issues/47893) shows that it isn't.
##
livenessProbeHeaders:
 - name: X-Forwarded-Proto
   value: https
readinessProbeHeaders:
 - name: X-Forwarded-Proto
   value: https
#livenessProbeHeaders: {}
#readinessProbeHeaders: {}

## Kubernetes configuration
## For minikube, set this to NodePort, elsewhere use LoadBalancer or ClusterIP
##
service:
  type: LoadBalancer
  ## HTTP Port
  ##
  port: 80
  ## HTTPS Port
  ##
  httpsPort: 443
  ## HTTPS Target Port
  ## defaults to https unless overridden to the specified port.
  ## if you want the target port to be "http" or "80" you can specify that here.
  ##
  httpsTargetPort: https
  ## Metrics Port
  ##
  metricsPort: 9117
  ## Node Ports to expose
  ## nodePorts:
  ##   http: <to set explicitly, choose port between 30000-32767>
  ##   https: <to set explicitly, choose port between 30000-32767>
  ##   metrics: <to set explicitly, choose port between 30000-32767>
  nodePorts:
    http: ""
    https: ""
    metrics: ""
  ## Enable client source IP preservation
  ## ref http://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/#preserving-the-client-source-ip
  ##
  externalTrafficPolicy: Cluster
  annotations: {}
  ## Limits which cidr blocks can connect to service's load balancer
  ## Only valid if service.type: LoadBalancer
  loadBalancerSourceRanges: []
  ## Extra ports to expose (normally used with the `sidecar` value)
  # extraPorts:

## Configure the ingress resource that allows you to access the
## WordPress installation. Set up the URL
## ref: http://kubernetes.io/docs/user-guide/ingress/
##
ingress:
  ## Set to true to enable ingress record generation
  ##
  enabled: true

  ## Set this to true in order to add the corresponding annotations for cert-manager
  ##
  certManager: false

  ## When the ingress is enabled, a host pointing to this will be created
  ##
  hostname: wordpress.local

  ## Ingress annotations done as key:value pairs
  ## For a full list of possible ingress annotations, please see
  ## ref: https://github.com/kubernetes/ingress-nginx/blob/master/docs/user-guide/nginx-configuration/annotations.md
  ##
  ## If certManager is set to true, annotation kubernetes.io/tls-acme: "true" will automatically be set
  ##
  annotations: {}

  ## The list of additional hostnames to be covered with this ingress record.
  ## Most likely the hostname above will be enough, but in the event more hosts are needed, this is an array
  ## extraHosts:
  ## - name: wordpress.local
  ##   path: /

  ## The tls configuration for additional hostnames to be covered with this ingress record.
  ## see: https://kubernetes.io/docs/concepts/services-networking/ingress/#tls
  extraTls:
   - hosts:
       - wordpress.local
     secretName: wordpress.local-tls

  ## If you're providing your own certificates, please use this to add the certificates as secrets
  ## key and certificate should start with -----BEGIN CERTIFICATE----- or
  ## -----BEGIN RSA PRIVATE KEY-----
  ##
  ## name should line up with a tlsSecret set further up
  ## If you're using cert-manager, this is unneeded, as it will create the secret for you if it is not set
  ##
  ## It is also possible to create and manage the certificates outside of this helm chart
  ## Please see README.md for more information
  ##
  secrets:
  -
    certificate: >
        -----BEGIN CERTIFICATE-----
        MIIDxzCCAq+gAwIBAgIUKqLXcfDRHKmie1HOyjraqG+BBvAwDQYJKoZIhvcNAQELBQAwczELMAkGA1UEBhMCRlIxEzARBgNVBAgMClNvbWUtU3RhdGUxDjAMBgNVBAcMBVBhcmlzMSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQxCzAJBgNVBAsMAm1yMQ8wDQYDVQQDDAZjb25uYWwwHhcNMjAwMzA3MTQ0NjExWhcNMjEwMzA3MTQ0NjExWjBzMQswCQYDVQQGEwJGUjETMBEGA1UECAwKU29tZS1TdGF0ZTEOMAwGA1UEBwwFUGFyaXMxITAfBgNVBAoMGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDELMAkGA1UECwwCbXIxDzANBgNVBAMMBmNvbm5hbDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKpxzxREi8s/mblAl28CiBFdn90h5XIq5X6KUFcGUopBFnOSLvO8VZWyo3cNP6R1wy5DuYFoI0Bf8eM1K/vT1QyNJQ1k0VMOuRezhQhkh7JXt9Wb1AbTIYGZfwwIUYJFrITV77jGyX0aBx4lI7/ME9MeUcnAMZM/dbiueWPngEfo4P3BLt/aDGB0MSb28rcVzuq0AqjORDgz/zsNn6KUzKOTOI7XTb5NJhuJOahCHYG7gDJ1zTkydTup/XYAeXLbZWDtN9kFF6KX3nPWXIZgqMOANxdxBXtSrUd7SiWie0sce1skbwO+UrBx743RtOo0ajtlYOXFoaGdySPCpDNN/T0CAwEAAaNTMFEwHQYDVR0OBBYEFDHa74WQ5O5QJCkECXuBxiWVtmIeMB8GA1UdIwQYMBaAFDHa74WQ5O5QJCkECXuBxiWVtmIeMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAGBfDY/ro/pdG0nzUpbK7yn4zobmGfS4E825L+h7cZ5n3OM8UOQ6BQc16EJbph/G2H1cle6HQgdP9j3GHJNRSTyoFPuxOokanwCwR2me53b3APCX02cI0PcyeNVrRVNfDkvKWQTjmlwzKWsu+hXJkPaSnDPrtiuUv++eoWM83Nt77TxanZG0df8QuYYT21NN2ebDHY9hRI/Bf+95/zpU9f3Oh2fXnb7uIr6pghYehDvfdrh5BzZWt2MdTGWSbi5RCbaTPSqN1RCY4+I+FhsQlEhYEzHm1Csz0VsKAWKTpNaiw1W4ArkGM/dVEq5cY3IfPtB5Fn5PwfleERVTCoPBZfY=
        -----END CERTIFICATE-----
    key: |
        -----BEGIN PRIVATE KEY-----
        MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCqcc8URIvLP5m5QJdvAogRXZ/dIeVyKuV+ilBXBlKKQRZzki7zvFWVsqN3DT+kdcMuQ7mBaCNAX/HjNSv709UMjSUNZNFTDrkXs4UIZIeyV7fVm9QG0yGBmX8MCFGCRayE1e+4xsl9GgceJSO/zBPTHlHJwDGTP3W4rnlj54BH6OD9wS7f2gxgdDEm9vK3Fc7qtAKozkQ4M/87DZ+ilMyjkziO102+TSYbiTmoQh2Bu4Aydc05MnU7qf12AHly22Vg7TfZBReil95z1lyGYKjDgDcXcQV7Uq1He0olontLHHtbJG8DvlKwce+N0bTqNGo7ZWDlxaGhnckjwqQzTf09AgMBAAECggEAL2VWYBxvgyU8afc8JPJfQ/T071jtpMGo73cmwAxSB3/x7lFIHR7R7b6vFfvpJqJYlBThEMsUgmIrsTKn+G7/X24gT4kpbB87+QtCgwLxePZLeGrMmtk7B/7XBVGK2N7nCkkzT/53KQKD2C4bssnc+WgE9kUrg/axlZluyAkn8taq+0xbPx7CJ9cRGVssbuJiUJgo94Ew7WAD2HoaOcXlAEbAJWnG9UulWyKNUFQUZHr4DzitanjRf2drMTwjGWkfkf5Q2uwEpc3cpKz6IHyKmx8nIFs24n2FAbjftyjCsNeam7fh+Yr9Gi0CHEnhNWn3+d/Ni3RGNCOr+1mZU3idQQKBgQDYWDqI4Zou1zUfKqGCa+aTL/om8Ks7U+gNDvtSCMLTG/cMHgSaoKCsjMPWvLieD5qvN1Df0vDyCpDCB44fWXHA9tSr5Un3fDA7Y4CemuIQBEcre9uJomnLtTfj/w/1pqwklX0r1R0ZPybyZdL0apzkGqS0uNfP9uMhoXTqMCLd9wKBgQDJr8MEOwS8Z2ctLO8XmPeze98IrPsiT0S8j3GQEzAqcL6OQ7tgMWp4kDu3KgAaojxJFW4+8Us70+xbOcAOuisIgCbkJwUTw0BFFD7VVJZ9cYtGgFrx4LbhIensx9R2ctjqZExEgrttg5TDCbWEDFvGWtsXAKBBB2pp8FtfBrzBawKBgDhPbpzmK515gmgkTnmF4DP7Tt8H0WnVwxZzCdMmWJ9eLgVHYudkW63NzcN8x4jMgvSML06ytugo+9ik4sciHsuc82nyejgiaONsWLPvmFuW4Xdd+5xtW6AcAPolD2BfC3tSNYxNIg/KMjbxbeEURtXQYYJwgiutFR1Ryv6RnOGtAoGAbKfHDYFqNMIUYM8jY/giAk0KxFbE2SARbPBugusDeOnSjzxINzOII7m4oh37fl3G0qy7/ybpnCq43BDJumVrh6Ha0fF/l70J4+1bSHzQsnY4TVwbItIoVDN8HfcuLD4FRImSWRqBNSe9PjH920z5KGyMOeE5dK81EivOXex4Rk8CgYByYb/V74Slh0h5z90ZHSb8StZr0KGXvbDuBHkAVdFThWSBAXJwHUJKY93oSOzoQqsIMeSUoqT7ohCt/yBlPxOZz/JN/q8EplFtERo64lzzt+8iLN1Q0gjvM/SQWIyp9v7wUKCu7g3L4t2HoFVJWDFOqabCpbAxqD1UFqh7pFdvaQ==
        -----END PRIVATE KEY-----
    name: wordpress.local-tls
## Enable persistence using Persistent Volume Claims
## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
##
persistence:
  enabled: true
  ## wordpress data Persistent Volume Storage Class
  ## If defined, storageClassName: <storageClass>
  ## If set to "-", storageClassName: "", which disables dynamic provisioning
  ## If undefined (the default) or set to null, no storageClassName spec is
  ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
  ##   GKE, AWS & OpenStack)
  ##
  # storageClass: "-"
  ##
  ## If you want to reuse an existing claim, you can pass the name of the PVC using
  ## the existingClaim variable
  # existingClaim: your-claim
  accessMode: ReadWriteOnce
  size: 10Gi

##
## MariaDB chart configuration
##
## https://github.com/helm/charts/blob/master/stable/mariadb/values.yaml
##
mariadb:
  ## Whether to deploy a mariadb server to satisfy the applications database requirements. To use an external database set this to false and configure the externalDatabase parameters
  enabled: false
  ## Disable MariaDB replication
  replication:
    enabled: false

  ## Create a database and a database user
  ## ref: https://github.com/bitnami/bitnami-docker-mariadb/blob/master/README.md#creating-a-database-user-on-first-run
  ##
  db:
    name: bitnami_wordpress
    user: bn_wordpress
    ## If the password is not specified, mariadb will generates a random password
    ##
    # password:

  ## MariaDB admin password
  ## ref: https://github.com/bitnami/bitnami-docker-mariadb/blob/master/README.md#setting-the-root-password-on-first-run
  ##
  # rootUser:
  #   password:

  ## Enable persistence using Persistent Volume Claims
  ## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
  ##
  master:
    persistence:
      enabled: true
      ## mariadb data Persistent Volume Storage Class
      ## If defined, storageClassName: <storageClass>
      ## If set to "-", storageClassName: "", which disables dynamic provisioning
      ## If undefined (the default) or set to null, no storageClassName spec is
      ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
      ##   GKE, AWS & OpenStack)
      ##
      # storageClass: "-"
      accessModes:
        - ReadWriteOnce
      size: 8Gi

##
## External Database Configuration
##
## All of these values are only used when mariadb.enabled is set to false
##
externalDatabase:
  ## Database host
  ##
  host: terraform-20200306164755256200000001.cdhwmnptb4kx.eu-west-3.rds.amazonaws.com

  ## non-root Username for Wordpress Database
  ##
  user: admin

  ## Database password
  ##
  password: changeme

  ## Database name
  ##
  database: artifaktWordpress

  ## Database port number
  ##
  port: 3306

## Prometheus Exporter / Metrics
##
metrics:
  enabled: false
  image:
    registry: docker.io
    repository: bitnami/apache-exporter
    tag: 0.7.0-debian-10-r32
    pullPolicy: IfNotPresent
    ## Optionally specify an array of imagePullSecrets.
    ## Secrets must be manually created in the namespace.
    ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
    ##
    # pullSecrets:
    #   - myRegistryKeySecretName
  ## Metrics exporter pod Annotation and Labels
  podAnnotations:
    prometheus.io/scrape: "true"
    prometheus.io/port: "9117"

  ## Metrics exporter containers' resource requests and limits
  ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
  ##
  resources:
    limits: {}
    requests: {}

  ## Prometheus Service Monitor
  ## ref: https://github.com/coreos/prometheus-operator
  ##      https://github.com/coreos/prometheus-operator/blob/master/Documentation/api.md#endpoint
  serviceMonitor:
    ## If the operator is installed in your cluster, set to true to create a Service Monitor Entry
    enabled: false
    ## Specify the namespace in which the serviceMonitor resource will be created
    # namespace: ""
    ## Specify the interval at which metrics should be scraped
    interval: 30s
    ## Specify the timeout after which the scrape is ended
    # scrapeTimeout: 30s
    ## Specify Metric Relabellings to add to the scrape endpoint
    # relabellings:
    ## Specify honorLabels parameter to add the scrape endpoint
    honorLabels: false
    ## Used to pass Labels that are used by the Prometheus installed in your cluster to select Service Monitors to work with
    ## ref: https://github.com/coreos/prometheus-operator/blob/master/Documentation/api.md#prometheusspec
    additionalLabels: {}

## Add sidecars to the pod.
## Example:
## sidecars:
##   - name: your-image-name
##     image: your-image
##     imagePullPolicy: Always
##     ports:
##       - name: portname
##         containerPort: 1234
##
sidecars: {}
