terraform {
  backend "local" {
    path = "./terraform.tfstate"
  }

  required_version = "0.12.9"
}

provider "aws" {
  region = "eu-west-3"
}

module "eks" {
  source = "../../resources/aws"
}
