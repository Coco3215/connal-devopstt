resource "aws_security_group" "cluster" {
  name = "${var.name}-cluster"
  description = "Coms to the masters"
  #Already have id but wouldn't normally so grabbing from data block
  vpc_id = data.aws_vpc.default.id
}

#Pretty sure masters need to talk back to AWS so need egress wide open
resource "aws_security_group_rule" "cluster-egress" {
  description = "master out coms"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.cluster.id
  type = "egress"
}

resource "aws_security_group_rule" "cluster-node" {
  description = "node to master coms"
  from_port = 443
  to_port = 443
  protocol = "tcp"
  source_security_group_id = aws_security_group.cluster.id
  security_group_id = aws_security_group.cluster.id
  type = "ingress"
}

# Will probably need to add ingress to configure helm or kubectl
resource "aws_security_group_rule" "cluster-local" {
  description = "local to master coms"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = [var.local_cidr]
  security_group_id = aws_security_group.cluster.id
  type = "ingress"
}

resource "aws_security_group" "node" {
  name = "${var.name}-node"
  description = "coms for nodes"
  vpc_id = data.aws_vpc.default.id
}

resource "aws_security_group_rule" "node-egress" {
  description = "node out coms"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.node.id
  type = "egress"
}

resource "aws_security_group_rule" "node_node" {
  description = "node to node coms"
  from_port = 0
  to_port = 65535
  protocol = "-1"
  security_group_id = aws_security_group.node.id
  source_security_group_id = aws_security_group.node.id
  type = "ingress"
}

resource "aws_security_group_rule" "node_cluster" {
  description = "master to node coms"
  from_port = 0
  to_port = 65535
  protocol = "tcp"
  security_group_id = aws_security_group.node.id
  source_security_group_id = aws_security_group.cluster.id
  type = "ingress"
}

resource "aws_security_group" "database" {
  name = "${var.name}-db"
  description = "coms for nodes to db"
  vpc_id = data.aws_vpc.default.id
}

resource "aws_security_group_rule" "db-egress" {
  description = "db out coms"
  from_port = 0
  to_port = 0
  protocol = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.database.id
  type = "egress"
}

resource "aws_security_group_rule" "db_node" {
  description = "node to db coms"
  from_port = 3306
  to_port = 3306
  protocol = "-1"
  cidr_blocks = [data.aws_vpc.default.cidr_block]
  security_group_id = aws_security_group.database.id
  type = "ingress"
}
