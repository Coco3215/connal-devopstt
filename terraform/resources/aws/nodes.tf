resource "aws_eks_node_group" "artifakt" {
  cluster_name = aws_eks_cluster.artifakt.name
  node_group_name = var.name
  node_role_arn = aws_iam_role.node.arn
  subnet_ids = data.aws_subnet_ids.default.ids
  scaling_config {
    desired_size = 3
    max_size = 3
    min_size = 3
  }

  depends_on = [
    "aws_iam_role_policy_attachment.eks_cni_policy",
    "aws_iam_role_policy_attachment.eks_ecr_policy",
    "aws_iam_role_policy_attachment.eks_worker_policy"
  ]
}
