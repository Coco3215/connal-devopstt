data "aws_vpc" "default" {
  id = var.vpc_id
}

data "aws_subnet_ids" "default" {
  vpc_id = var.vpc_id
}

data "aws_ami" "eks_node" {
  filter {
    name = "name"
    values = ["amazon-eks-node-${aws_eks_cluster.artifakt.version}-v*"]
  }

  most_recent = true
  owners = ["602401143452"] #AWS's account number
}

data "aws_region" "current" {}
