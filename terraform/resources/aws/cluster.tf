resource "aws_eks_cluster" "artifakt" {
  name = var.name
  role_arn = aws_iam_role.cluster.arn

  vpc_config {
    security_group_ids = [aws_security_group.cluster.id]
    subnet_ids = data.aws_subnet_ids.default.ids
  }

  depends_on = [
    "aws_iam_role_policy_attachment.eks_service_policy",
    "aws_iam_role_policy_attachment.eks_cluster_policy"
  ]
}

# Creates local kubeconfig so we can run kubectl against the cluster
resource "null_resource" "create_kubekconfig" {
  provisioner "local-exec" {
    command = "echo '${local.kubeconfig}' >> ~/.kube/config"
  }
}
