# Small RDS instance for wordpress.
# Paswords should not be handled like this!!
resource "aws_db_instance" "wordpress" {
  allocated_storage = 20
  storage_type = "gp2"
  engine = "mysql"
  engine_version = "5.7"
  instance_class = "db.t2.micro"
  name = "artifaktWordpress"
  username = "admin"
  password = "changeme"
  parameter_group_name = "default.mysql5.7"
  vpc_security_group_ids = [aws_security_group.database.id]
  skip_final_snapshot = true
}
