variable "name" {
  default = "artifakt-tt"
  description = "Name to prefix everything with for consistancy"
}

variable "vpc_id" {
  default = "vpc-7c998d15"
  description = "VPC to deploy into, I deployed into default VPC for ease. I would not reccomend this."
}

variable "local_cidr" {
  default = "82.226.154.120/32"
  description = "Local CIDR to punch a hole in the master Security groups"
}
