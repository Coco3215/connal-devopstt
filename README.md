# Hello

## Prerequisites
You need to have the following installed to run the code contained in this repo.
- terraform 0.12.9 ([tfenv](https://github.com/tfutils/tfenv) is super handy for this)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- [helm](https://helm.sh/docs/intro/install/)
- [docker](https://docs.docker.com/install/)
- [aws-iam-authenticator](https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html)

## Intro
In order to run the wordpress site on kubernetes I decided to use AWS EKS. I have prior experience using EKS so it would make it easier for me to use. All infrastructure is created using terraform :heart:. Helm is the orchestrater for the Wordpress contianer. I figured using a chart with db passwords etc would be easiest to replicate again. **DB passwords should not live in the chart**. Please note this setup is region dependent.

## Deploy the infrastructure
1. I used a local credential called `default` to authenticate with AWS. So make sure you have a credential in `~/.aws/credentials` called default and that is for the AWS account you want to deply to.
2. `cd terraform/environments/test`
3. `terraform init`
4. `terraform apply`
5. Check kubernetes is up and all nodes are connected. I used some terraform local-exec resources to setup the local kubectl resources for me.

## Deploying WordPress
I decided to use a premade HELM chart to save some time, It also had lots of the SSL/Ingress headache stuff built in. You can find the chart repo [here](https://github.com/helm/charts/tree/master/stable/wordpress). Whilst doing this I found a bug a in the chart which is why I used to chart locally with the bug fix.

The only manual step before running install on the helm chart is to get the value for the DB Host from the AWS Console and add it to the helm-values.yaml file. Its near the bottom of the file.

I only had one local kubeconfig setup but make sure if you have multiple and use something like kubectx you are pointing at the right cluster.
Then you can run:
```
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm install connal -f helm-values.yalm ../wordpress-helm
```

To grab the URL of the wordpress svc run
`kubectl get svc`
the url for connal-wordpress is what you need. I found the DNS very slow to resolve to this so it can take a while, if you are impatient like me nslookup `nslookup <url> 8.8.8.8` is faster and gives you an IP for the LB

# Problems
Minus the SSL/TLS everything works. For some reason Apache doesn't like the key and cert I gave it. (maybe I pasted them in wrong) but if you remove the section around tls in `helm-values.yaml` you can see it all spin up and work. I decided against debugging this further as its not a production issue and its getting late, I hope you understand.

# Technical Goal 4
I didn't really do this one, I kinda disagreed with doing it. Between the use of helm, kubectl and the fact the container pulls the repo at runtime I don't think it is a good idea to make a scripted way to interact with containers.
Using helm you should be able to make any config changes as need.
With kubectl you could make small changes to test.
With git you can add wordpress config as you need.
I've always believed in not interacting with the nodes, this way no manual config happens and we lower our attack vector.

#Questions
- TIME: 2 Afternoons (wasted a lot of time getting my local env set up)
- Languages: Terraform because I know it so well and making infrastructure in any cloud is really simple. Bash again quick and easy no extra requirements etc.
- CLOUD: AWS. I've mainly worked in AWS in the past so I know my way around an AWS account pretty well. Also I've wanted to take a 2nd look at EKS for a while to see if it has improved since I last used it.
- INTRO: from the base of the repo run `./connal.sh`
